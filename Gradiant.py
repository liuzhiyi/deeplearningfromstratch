#!/usr/bin/env python
"""This script define gradiant decent function"""
import numpy as np
import DpUtils as dp

def numerical_gradiant(func, val):
    """Return the numerical gradiant when x = val."""
    h = 1e-4 #0.0001
    h2 = 2*h
    grad = np.zeros_like(val) #產生和 x 相同形狀的矩阵
    for i in range(val.size):
        tmp_val = val[i]
        val[i] = tmp_val + h
        fxh1 = func(val)
        val[i] = tmp_val - h
        fxh2 = func(val)

        grad[i] = (fxh1 - fxh2) / h2
        val[i] = tmp_val
    return grad

def descent(f, init_x, lr=0.01, step_num=100, f_gradiant=numerical_gradiant):
    """do gradiant decent"""
    x = init_x

    for i in range(step_num):
        grad = f_gradiant(f, x)
        x -= lr * grad

    return x

class simpleNet:
    "2x3 simple neural network"
    def __init__(self):
        self.W = np.random.randn(2, 3)

    def predict(self, x):
        """正向传输"""
        return np.dot(x, self.W)

    def loss(self, x, t):
        """损失函数"""
        z = self.predict(x)
        y = dp.softmax(z)
        loss = dp.cross_entropy_error(y, t)
        return loss
