#!/usr/bin/env python
"""This script define utilities for deep leaning"""
import numpy as np

def softmax(a):
    """softmax 返回0～1之间的实数值，相当于概率"""
    c = np.max(a)
    exp_a = np.exp(a - c) # prevent overflow
    sum_exp_a = np.sum(exp_a)
    y = exp_a / sum_exp_a
    return y

def sigmoid(a):
    """sigmoid平滑活化函数"""
    return 1 / (1 + np.exp(-a))

def relu(a):
    """ReLU(Rectified Linear Unit)活化函数 h(x)=x,if x>0 h(x)=0,if x<=0"""
    return np.maximum(0, a)

def mean_squared_error(y, t):
    """均方误差"""
    return 0.5 * np.sum((y - t) ** 2)

def cross_entropy_error(y, t):
    "交叉熵误差"
    delta = 1e-7
    return -1 * np.sum(t * np.log(y + delta))
