import numpy as np
import Gradiant

def function2(x):
    return np.sum(x**2)

def function_2(x):
    return x[0]**2 + x[1]**2

def main():
    #result = Gradiant.numerical_gradiant(function2, np.array([3.0,4.0]))
    init_x = np.array([-6.0, 5.0])
    result = Gradiant.descent(function_2, init_x=init_x, lr=10, step_num=300)
    print(result)
    return

if __name__ == "__main__":
    main()