import tensorflow as tf
from numpy.random import RandomState
import DpUtils as dp

def main():
    batch_size = 8

    w1 = tf.Variable(tf.random_normal([2, 3], stddev=1, seed=1))
    w2 = tf.Variable(tf.random_normal([3, 1], stddev=1, seed=1))

    x = tf.placeholder(tf.float32, shape=(None, 2), name = 'x-input')
    y__ = tf.placeholder(tf.float32, shape=(None, 1), name = 'y-input')

    a = tf.matmul(x, w1)
    y = tf.matmul(a, w2)

    #cross_entropy = dp.cross_entropy_error(y, y__)
    #train_step = tf.train.AdamOptimizer(0.001).minimize(cross_entropy)

    rdm = RandomState(1)
    dataset_size = 128
    X = rdm.rand(dataset_size, 2)
    Y = [[int(x1 + x2 < 1)] for (x1, x2) in X]

    with tf.Session as sess:
        init_op = tf.initialize_all_variables()
        sess.run(init_op)
        print(sess.run(w1))
        print(sess.run(w2))
    return

if __name__ == "__main__":
    main()
